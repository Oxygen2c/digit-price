import Vue from "vue";
import VueRouter from "vue-router";
import Order from "../views/Order.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/order/:id",
    name: "Order",
    component: Order
  },
  {
    path: "*",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Error404.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
