import { http } from "@/boot/axios";
// const headers = {'referrerPolicy': 'no-referrer'}
export async function getOrder({ commit }, { id, lang }) {
  commit("toggleKey", { key: 'loading', value: true })
  try {
    const {
      data,
    } = await http.get(
      `http://139.59.15.12:8080/test`,
      { params: { sku: id, lang } }
    );
    commit("setOrder", data);
    commit("toggleKey", { key: 'loading', value: false })
    commit("toggleKey", { key: 'isError', value: false })
  } catch (e) {
    commit("toggleKey", { key: 'loading', value: false })
    commit("toggleKey", { key: 'isError', value: true })
    throw new Error("Ошибка getOrder()");
  }
}