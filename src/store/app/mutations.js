export function setTheme(state, val) {
  state.theme = val
}

export function setOrder(state, data) {
  state.order = data
}

export function toggleKey(state, { key, value }) {
  state[key] = value
}