export const Order = (state) => state.order;
export const Theme = (state) => state.theme;
export const Loading = (state) => state.loading;
export const isError = (state) => state.isError;
