import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueI18n from 'vue-i18n'
import { BootstrapVue } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/styles/main.scss'

Vue.use(BootstrapVue)
Vue.use(VueI18n)
Vue.config.productionTip = false;

const messages = {
  kk: {
    credit: 'Бөліп төлеу',
    yearTariff: 'тариф бір жылға',
    month: 'ай',
    noTariffPrice: 'Тарифсіз бағасы',
    features: 'Сипаттамасы',
    whatTariff: 'Жиынтықта қандай тариф',
    tariffTextOne: 'Бөліп төлеуді ресімдегенде, жиынтықта тариф жоспарын қоса аласыз.',
    tariffTextTwo: 'Тарифке кіреді (ай сайын)',
    creditConditionTitle: 'Бөліп төлеу шарттары',
    creditBeeline: 'Beeline бөліп төлеу мүмкіндігін ұсынады',
    creditConditionText: 'Бөліп төлей аласыз, егер'
  },
  en: {
    credit: 'Рассрочка',
    yearTariff: 'тариф на год',
    month: 'мес',
    noTariffPrice: 'Цена без тарифа',
    features: 'Характеристики',
    whatTariff: 'Какой тариф в комплекте',
    tariffTextOne: 'При оформлении рассрочки Вы получаете тарифный план в комплекте.',
    tariffTextTwo: 'В тариф входит (ежемесячно)',
    creditConditionTitle: 'Условия рассрочки',
    creditBeeline: 'Рассрочка от',
    creditConditionText: 'Вы можете получить рассрочку, если'
  }
}

// Создание экземпляра VueI18n с настройками
const i18n = new VueI18n({
  locale: 'en', // установка локализации по умолчанию
  messages // установка сообщений локализаций
})

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
